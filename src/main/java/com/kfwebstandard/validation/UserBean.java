package com.kfwebstandard.validation;

import java.io.Serializable;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named("user")
@SessionScoped
public class UserBean implements Serializable {

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String newValue) {
        name = newValue;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int newValue) {
        age = newValue;
    }
}
